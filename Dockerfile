FROM registry.gitlab.com/thallian/docker-php7-fpm:master

ENV FPMUSER nginx
ENV FPMGROUP nginx

ENV VERSION master

RUN apk add --no-cache  \
    libressl \
    git \
    nginx \
    wget \
    php7 \
    php7-opcache \
    php7-pcntl \
    php7-gd \
    php7-sqlite3 \
    php7-json \
    php7-intl \
    php7-xml \
    php7-mbstring \
    php7-zip \
    php7-openssl \
    php7-phar \
    php7-pdo_sqlite \
    php7-ctype \
    php7-xmlwriter \
    php7-dom

RUN mkdir -p /usr/share/webapps/cops
RUN wget -qO- https://github.com/seblucas/cops/archive/$VERSION.tar.gz | tar xz -C /usr/share/webapps/cops --strip 1

WORKDIR /usr/share/webapps/cops
RUN wget https://getcomposer.org/composer.phar
RUN php composer.phar global require "fxp/composer-asset-plugin:~1.1"
RUN php composer.phar install --no-dev --optimize-autoloader

RUN chown -R nginx:nginx /usr/share/webapps/cops

RUN mkdir /run/nginx
RUN rm /etc/nginx/conf.d/default.conf

ADD /rootfs /

VOLUME /var/lib/cops/calibre
