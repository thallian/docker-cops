[COPS](https://github.com/seblucas/cops), a web-based light alternative to Calibre content server.

# Volumes
- `/var/lib/cops/calibre`

# Environment Variables
## DOMAIN
Domain where the cops instance is reachable.

## TITLE
- default: COPS

Website title.

# Ports
- 80
